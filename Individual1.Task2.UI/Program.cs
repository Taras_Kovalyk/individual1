﻿using Individual1.Task2.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task2.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            ExampleIllegalPoints();
            Rectangle rectangle1 = ExampleCreateAndGetPoints();
            ExampleWidthHeight(rectangle1);
            ExampleMove(rectangle1);
            ExampleUnionIntersect(rectangle1, new Rectangle(new Point(1, 1), new Point(10, 10)));

            Console.ReadKey();
        }

        private static void ExampleIllegalPoints()
        {
            try
            {
                Console.WriteLine("Try to create rectangle by 1 point: ");
                new Rectangle(new Point());
            }
            catch (IllegalPointsException e)
            {
                Console.WriteLine(e.Message);
            }

            try
            {
                Console.WriteLine("Try to create rectangle by non-parallel lines: ");
                new Rectangle(new Point(2, 5), new Point(1, 1), new Point(5, 5), new Point(8, 8));
            }
            catch (IllegalPointsException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static Rectangle ExampleCreateAndGetPoints()
        {
            Rectangle rectangle = new Rectangle(new Point(2, 2), new Point(8, 4));

            Console.WriteLine();
            Console.WriteLine("We can access the point of the rectangle, as example 3rd point is: ");
            Console.WriteLine(rectangle[3]);

            return rectangle;
        }

        private static void ExampleWidthHeight(Rectangle rectangle)
        {
            Console.WriteLine();
            Console.WriteLine("Height of the rectangle is: " + rectangle.Height);
            Console.WriteLine("Width of the rectangle is: " + rectangle.Width);

            rectangle.IncreaseHeight(2);
            Console.WriteLine("Height of the rectangle after increase is: " + rectangle.Height);
            rectangle.IncreaseWidth(4);
            Console.WriteLine("Width of the rectangle after increase is: " + rectangle.Width);
        }

        private static void ExampleMove(Rectangle rectangle)
        {
            Console.WriteLine();
            Console.WriteLine("Current coordinates are: ");
            Console.WriteLine(rectangle);

            Console.WriteLine("After moving top: ");
            rectangle.Move(MoveDirection.Top, 2);
            Console.WriteLine(rectangle);
        }

        private static void ExampleUnionIntersect(Rectangle r1, Rectangle r2)
        {
            Console.WriteLine();
            Console.WriteLine("Example of union: ");
            Console.WriteLine(Rectangle.Union(r1, r2));

            Console.WriteLine();
            Console.WriteLine("Example of intersection: ");
            Console.WriteLine(Rectangle.Intersect(r1, r2));
        }
    }
}
