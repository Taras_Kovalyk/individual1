﻿using Individual1.Task3.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task3.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            Polynomial polynomial = ExampleCreate();
            ExampleOperations(polynomial, new Polynomial(new PolynomialMember(3, 3)));
            ExampleCalculate(polynomial);

            Console.ReadKey();
        }

        private static Polynomial ExampleCreate()
        {
            Polynomial polynomial = new Polynomial();

            polynomial.AddMember(new PolynomialMember(3, 2));
            polynomial.AddMember(new PolynomialMember(-1, 2));
            polynomial.AddMember(new PolynomialMember(2, 2));

            Console.WriteLine("Polynomial: ");
            Console.Write(polynomial);

            return polynomial;
        }

        private static void ExampleOperations(Polynomial p1, Polynomial p2)
        {
            Console.WriteLine();
            Console.WriteLine("Addition of polynomials: ");
            Console.Write(p1 + p2);

            Console.WriteLine();
            Console.WriteLine("Subtraction of polynomials: ");
            Console.Write(p1 - p2);

            Console.WriteLine();
            Console.WriteLine("Multipling of polynomials: ");
            Console.Write(p1 * p2);
        }

        private static void ExampleCalculate(Polynomial polynomial)
        {
            Console.WriteLine();
            Console.WriteLine("Value of the polynomial with x=5: ");
            Console.Write(polynomial.Calculate(5));
        }
    }
}
