﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task4.Core
{
    public class Matrix
    {
        private double[,] _matrix;

        public Matrix(double[,] matrix)
        {
            _matrix = matrix;
        }

        public int Rows
        {
            get
            {
                return _matrix.GetLength(0);
            }
        }

        public int Columns
        {
            get
            {
                return _matrix.GetLength(1);
            }
        }

        private void AssertCorrectIndexes(int i, int j)
        {
            if (i < 0 || i >= Rows || j < 0 || j >= Columns)
            {
                throw new IndexOutOfRangeException();
            }
        }

        public double this[int i, int j]
        {
            get
            {
                AssertCorrectIndexes(i, j);

                return _matrix[i, j];
            }

            set
            {
                AssertCorrectIndexes(i, j);

                _matrix[i, j] = value;
            }
        }

        public Matrix GetMinor(int i, int j)
        {
            AssertCorrectIndexes(i, j);

            double[,] minor = new double[Rows - 1, Columns - 1];
            bool wasMinorColumn = false;
            bool wasMinorRow = false;

            for (int k = 0; k < Rows; k++)
            {
                if (k == i)
                {
                    wasMinorRow = true;
                    continue;
                }

                for (int l = 0; l < Columns; l++)
                {
                    if (l == j)
                    {
                        wasMinorColumn = true;
                        continue;
                    }

                    int newRow = wasMinorRow ? k - 1 : k;
                    int newColumn = wasMinorColumn ? l - 1 : l;

                    minor[newRow, newColumn] = _matrix[k, l];
                }
            }

            return new Matrix(minor);
        }

        public Matrix GetMinor(int k)
        {
            if (Rows < k || Columns < k)
            {
                throw new SizeException("K is greater than count of rows or columns");
            }

            Random random = new Random();
            int[] rowsIndexes = Enumerable
                .Range(0, Rows - 1)
                .OrderBy(n => random.Next())
                .Take(k)
                .ToArray();

            int[] columnsIndexes = Enumerable
                .Range(0, Columns - 1)
                .OrderBy(n => random.Next())
                .Take(k)
                .ToArray();

            double[,] newMatrix = new double[k, k];

            for (int i = 0; i < rowsIndexes.Length; i++)
            {
                for (int j = 0; j < columnsIndexes.Length; j++)
                {
                    newMatrix[i, j] = _matrix[rowsIndexes[i], columnsIndexes[j]];
                }

                i++;
            }

            return new Matrix(newMatrix);
        }

        public static bool AreSizeEquals(Matrix m1, Matrix m2)
        {
            return m1.Rows == m2.Rows && m1.Columns == m2.Columns;
        }

        public static bool CanMultiplicate(Matrix m1, Matrix m2)
        {
            return m1.Columns == m2.Rows;
        }

        public static Matrix operator+(Matrix m1, Matrix m2)
        {
            if (!AreSizeEquals(m1, m2))
            {
                throw new SizeException("Can't subtract, size is different");
            }

            double[,] newMatrix = new double[m1.Rows, m1.Columns];

            for (int i = 0; i < m1.Rows; i++)
            {
                for (int j = 0; j < m1.Columns; j++)
                {
                    newMatrix[i, j] = m1[i, j] + m2[i, j];
                }
            }

            return new Matrix(newMatrix);
        }

        public static Matrix operator -(Matrix m1, Matrix m2)
        {
            if (!AreSizeEquals(m1, m2))
            {
                throw new SizeException("Can't subtract, size is different");
            }

            double[,] newMatrix = new double[m1.Rows, m1.Columns];

            for (int i = 0; i < m1.Rows; i++)
            {
                for (int j = 0; j < m1.Columns; j++)
                {
                    newMatrix[i, j] = m1[i, j] - m2[i, j];
                }
            }

            return new Matrix(newMatrix);
        }

        public static Matrix operator *(Matrix m1, Matrix m2)
        {
            if (!CanMultiplicate(m1, m2))
            {
                throw new SizeException("");
            }

            double[,] newMatrix = new double[m1.Rows, m2.Columns];

            for (int i = 0; i < m1.Rows; i++)
            {
                for (int j = 0; j < m2.Columns; j++)
                {
                    for (int k = 0; k < m2.Rows; k++)
                    {
                        newMatrix[i, j] += m1[i, k] * m2[k, j];
                    }
                }
            }

            return new Matrix(newMatrix);
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                for(int j = 0; j < _matrix.GetLength(1); j++)
                {
                    builder.Append(_matrix[i, j] + "    ");
                }

                builder.Append(Environment.NewLine);
            }

            return builder.ToString();
        }
    }
}
