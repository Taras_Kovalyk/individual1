﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individual1.Task4.Core
{
    public class SizeException : Exception
    {
        public SizeException(string message) : base(message)
        { }

        public SizeException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
